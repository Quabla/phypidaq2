# PhyPiDAQ2 user interface consideration
PhyPiDAQ2 is the software stack for the PhyPiDAQ set of educational experimental equipment. Being an educational tool it should be capable of two
things.
1. It should show all necessary steps to run an "experiment" using modern equipment and methods, visualize the Results of the measurements in a
   compelling and understandable way and guide the user through the various steps reducing friction for the user as much as possible, without
   sacrificing the explanatory nature of the setup.
2. It should be open to exploration. This means that the software itself should be structured in such a way that it is easily understood by who ever
   looks into it to for example extend it's functionality or alter it's behaviour in some way. It should lend itself to extension and modification by
   e.g. providing hooks and a sensible application programming interface. A command line operation is also an option which would make it interoperable
   with other UNIX tools via common design idioms.

As a result of these two different goals the software needs to be able to operate in two different modes. Each mode targets one of the two cases
outlined above. The work that needs to be done to achieve the goals set out in point 2 concern mostly the structure of the application as that will
determine how much effort will need to be expended to implement a sensible API and (possibly on top of that) a set of hooks / a command line tool.

The considerations in point 1 can be achieved using a graphical user interface together with a set of constraints that limit what the user can and can
not do. This will need to assume that the user is familiar with common user interface design elements and patterns. The graphical user interface would
then be used to guide the user through the different steps to set up a series of pre-defined/pre-configured sensors/experiments. If the user wishes to
do more sophisticated things it is advisable to direct the user to the possibilities that lie with the API/command line interface.

### The GUI
As the graphical interface will be the interface that new and inexperienced users will come in contact with first, it is advisable to design it
carefully as to minimize unsatisfactory experiences. This means clear and concise feedback and error messages (especially those) that help identify
any problems together with a close to real time system state representation. Extensive use of graphical representations is advised as it increases
information "bandwidth" from the computer to the user.

### The CLI
The command line interface would be targeted at more advanced users and would facilitate "real" measurements of custom designed experimental setups.
To achieve this it should be able to read in configuration files and/or command line parameters or preferably both. The CLI should also use `stdin` and
`stdout` as is expected of well behaved Unix programs so that it can be combined with other Unix tools or user supplied scripts.

### The API
If the use of the CLI can no longer meet the needs of the user the user should have wide ranging and well structured access to the internal workings
of the PhyPiDAQ software. This will be facilitated by modular code with well defined data structures as interchange formats. The structure of the code
and that of the API should follow the Idea of a data analysis "pipeline" or put differently a "conveyor belt" concept where the data flows through/is
sent from one step to the next.

## GUI design
This document proposes to split the GUI into three main parts.
1. Setup: This part concerns itself with the setup and configuration of the sensors used in the experiment.
2. Data processing: This part shows the different transformations that are applied to the data and how they connect.
3. Data View: This part visualizes the data gathered by the sensors and configures the way the data is to be shown.

### Setup
This part is arguably the most difficult to get right. It has to convey the current state of the system and allow the user to make changes to that
state while keeping the user informed about error conditions and the possibilities to resolve them if (and when) they arise.

This step should only concern itself with getting the raw data and the state from the sensors of the system it should show the current
configuration/state of each sensor and allow the user to change these within reasonable bounds. This document considers calibration of raw data to be
a transformation which therefore does not belong to this part of the GUI

### Data processing:
This part of the application describes/declares the transformations that are applied to the raw data before it is visualized. This includes things
like calibration, outlier rejection and mathematical transformations. The user should be offered a range of 'data processing steps' which will be
called filters/maps in the following text and configure these within reasonable bounds. These filters/maps can be created configured and attached to
the existing data "pipeline"/"conveyor belt" to produce the data that is then to be visualized.

Visualizing this sort of flow will be especially tricky as it would be ideal to generate flow diagrams from the user input, aiding the understanding
of what is happening for the user.

### Data visualisation
This last part of the user interface would concern itself with the possibilities of visualizing the data. The user would need to choose the type of
plot together with the proper scaling and labels, deciding how the plot should look in the end without manipulating the data any further (as this was
already done in the data processing part. This should be fairly simple as there are only so many different common graphs and only so many options for
each graph. As this part would concern itself with visualisation the user would automatically see the results of configuration changes as a matter of
course.

## Conclusion
This document specifies the basic structure of all user facing components of PhyPiDAQ and outlines that the program be split into a common code base
consisting of libraries and modules that provide an API and are subsequently grouped into a command line tool for more advanced users and a GUI
application for more junior users. The layout of the GUI application is discussed in more detail and it is argued that the GUI application should be
split into three parts, namely the *Setup*, *Data processing* and *Data visualisation* part that all focus on one aspect of a full data analysis.
It is further specified that the options in the GUI should be intentionally limited as not to overwhelm the user and that the use of visualisations
is encouraged to enable easier understanding.
